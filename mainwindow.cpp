/*
  Mainwindow gestiona el proceso global de adquirir una imagen
  de una caja browser, extraer las subimágenes de los contadores
  y lanzar un OCR para leer los números.
*/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qdebug.h>
#include <cstdlib>
#include <iostream>   // std::cout
#include <string>
#include <boost/lexical_cast.hpp>



RNG rng(12345);

vector<vector<Point> > contours;
vector<Vec4i> hierarchy;

using namespace std;
// Constructor member
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    // Original Image and sub-images
    this->Image_Source = new QImage(720,540, QImage::Format_RGB888);
    this->image_counter = new QImage(304,130, QImage:: QImage::Format_Indexed8);
    this->image_counter2 = new QImage(304,130, QImage:: QImage::Format_Indexed8);
    this->image_counter3 = new QImage(304,130, QImage:: QImage::Format_Indexed8);

    // viewers
    viewer_original = new RCDraw(720,540, Image_Source, ui->viewer_original);
    viewer_counter1 = new RCDraw(304,130, image_counter, ui->viewer_counter1);
    viewer_counter2 = new RCDraw(304,130, image_counter2, ui->viewer_counter2);
    viewer_counter3 = new RCDraw(304,130, image_counter3, ui->viewer_counter3);




    // connect to button
    connect ( ui->Load_button, SIGNAL (clicked()), this, SLOT( Load_Image() ) );
    connect (ui->Clip_button, SIGNAL (clicked()), this, SLOT(on_clip_button_clicked()));
    connect (ui->OCR_button, SIGNAL(clicked()), this, SLOT(getNumbers()));


}
// Destructor member
MainWindow::~MainWindow()
{
    delete ui;

    delete Image_Source;
    delete image_counter;
    delete image_counter2;
    delete image_counter3;
    delete viewer_original;
    delete viewer_counter1;
    delete viewer_counter2;
    delete viewer_counter3;
}



void MainWindow::Umbralize(){

        threshold(mat_counter, mat_counter, 40, 255, 0);
        threshold(mat_counter2, mat_counter2, 40, 255, 0);
        threshold(mat_counter3, mat_counter3, 40, 255, 0);

}

void MainWindow::Load_Image()
{
    // Looking for a image name in a directory with browsing box
    QString Directory="capturas";
    QString fn = QFileDialog::getOpenFileName(this,"Choose a frame to download",Directory, "Images (*.png *.xpm *.jpg)");

    Original_Image= imread(fn.toStdString(), CV_LOAD_IMAGE_COLOR);
    cv::resize(Original_Image, mat_original, Size(720, 540), 0, 0, cv::INTER_CUBIC);
    memcpy(Image_Source->bits(),mat_original.data,mat_original.rows*mat_original.cols*sizeof(uchar)*3);
    viewer_original->update();

}


void MainWindow::on_clip_button_clicked()
{
    //Clip the image fragments so we can analyze them one by one

    //Regions of the image's bounds. Also called ROI(Region Of InterEX)
    Rect region1;
    Rect region2;
    Rect region3;

    region1.x=1020;
    region1.y = 575;
    region1.width = 427;
    region1.height = 125;

    region2.x=1020;
    region2.y = 858;
    region2.width = 427;
    region2.height = 125;

    region3.x=1020;
    region3.y = 1155;
    region3.width = 427;
    region3.height = 125;

    //Temporal mat
    Mat temp;

    //Resizing elements, then updating viewers

    //Clip 1
    temp = Original_Image(region1);
    cv::resize(temp,mat_counter,Size(304,130),0,0,cv::INTER_CUBIC);
    cvtColor(mat_counter,mat_counter,CV_BGR2GRAY);

    memcpy(image_counter->bits(),mat_counter.data, mat_counter.rows*mat_counter.cols*sizeof(uchar));
    viewer_counter1->update();

    //Clip 2

    temp = Original_Image(region2);
    cv::resize(temp,mat_counter2,Size(304,130),0,0,cv::INTER_CUBIC);
    cvtColor(mat_counter2,mat_counter2,CV_BGR2GRAY);

    memcpy(image_counter2->bits(),mat_counter2.data, mat_counter2.rows*mat_counter2.cols*sizeof(uchar));
    viewer_counter2->update();

    //Clip 3

    temp = Original_Image(region3);
    cv::resize(temp,mat_counter3,Size(304,130),0,0,cv::INTER_CUBIC);
    cvtColor(mat_counter3,mat_counter3,CV_BGR2GRAY);

    memcpy(image_counter3->bits(),mat_counter3.data, mat_counter3.rows*mat_counter3.cols*sizeof(uchar));
    viewer_counter3->update();



}

void MainWindow::getNumbers(){
       //Variables for the images of the numbers and the numbers themselves
        Mat numbers_img[4][3];
        int cifras[4][3];

        //Clipping numbers...
        numbers_img[0][0] = Original_Image(Rect(1017, 564, 112, 146));
        numbers_img[1][0] = Original_Image(Rect(1120, 564, 112, 146));
        numbers_img[2][0] = Original_Image(Rect(1222, 564, 112, 146));
        numbers_img[3][0] = Original_Image(Rect(1325, 564, 112, 146));

        numbers_img[0][1] = Original_Image(Rect(1017, 845, 112, 146));
        numbers_img[1][1] = Original_Image(Rect(1120, 845, 112, 146));
        numbers_img[2][1] = Original_Image(Rect(1222, 845, 112, 146));
        numbers_img[3][1] = Original_Image(Rect(1325, 845, 112, 146));

        numbers_img[0][2] = Original_Image(Rect(1017, 1146, 112, 146));
        numbers_img[1][2] = Original_Image(Rect(1120, 1146, 112, 146));
        numbers_img[2][2] = Original_Image(Rect(1222, 1146, 112, 146));
        numbers_img[3][2] = Original_Image(Rect(1325, 1146, 112, 146));

        for(int i = 0; i<4; i++){
            for(int j = 0; j<3; j++){
                cifras[i][j] = MatchNumbers(numbers_img[i][j]);
            }
        }

        float number1 = cifras[0][0]*100 + cifras[1][0]*10 + cifras[2][0] + cifras[3][0]*0.1;
        float number2 = cifras[0][1]*100 + cifras[1][1]*10 + cifras[2][1]+ cifras[3][1]*0.1;
        float number3 = cifras[0][2]*100 + cifras[1][2]*10 + cifras[2][2] + cifras[3][2]*0.1;

        //Stream to convert between int and string
        stringstream converter;

        converter<< number1;
        string str1 = converter.str();
        converter.str("");
        converter<< number2;
        string str2 = converter.str();
        converter.str("");
        converter << number3;
        string str3 = converter.str();

        ui->resultado1->setText(QString::fromStdString(str1));
        ui->resultado2->setText(QString::fromStdString(str2));
        ui->resultado3->setText(QString::fromStdString(str3));

}

int MainWindow::MatchNumbers(Mat number){
    //Variables
    Mat t, template_mat, compare_mat;
   // Point minLoc, maxLoc, matchLoc;
    char* filename;
    int match_method = CV_TM_CCORR_NORMED;
    int minI=10;
    int minNum=999999999;

    //Clone the parameter's Mat
     Mat m = number.clone();
     //Converting it to gray
     cvtColor(m,m,CV_RGB2GRAY);
    //Apply the threshold
     threshold(m,m,40,255,0);




     for (int i = 0; i<10; i++) {
        //Loop all patterns, apply them the color and threshold
        //Patterns should be located in the debug's build folder
         template_mat= imread( "templates/"+boost::lexical_cast<string>(i)+".png", CV_LOAD_IMAGE_COLOR);
         cvtColor(template_mat, template_mat, CV_BGR2GRAY);
         threshold(template_mat, template_mat, 40, 255, 0);
         //See if they are similar
         matchTemplate(m, template_mat, t, match_method);
         normalize(t, t, 0, 1, NORM_MINMAX, -1, Mat());
         //Resize and compare right after normalization
         cv::resize(template_mat, template_mat, Size(m.cols, m.rows), 0, 0, cv::INTER_CUBIC);
         cv::compare(template_mat,m,compare_mat, cv::CMP_NE);
        //If the similarity threshold is bigger than the minus of both mats, we've found the number in the patterns
         int noCero = countNonZero(compare_mat);
         if (noCero <= minNum) {
             minNum = noCero;
             minI = i;
         }

     }
     return minI;

}
