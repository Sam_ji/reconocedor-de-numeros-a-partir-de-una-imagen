/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QTextEdit>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QFrame *viewer_original;
    QLabel *Tittle;
    QPushButton *Load_button;
    QFrame *viewer_counter1;
    QLabel *label_2;
    QPushButton *Clip_button;
    QFrame *viewer_counter2;
    QLabel *label_3;
    QFrame *viewer_counter3;
    QLabel *label_4;
    QPushButton *OCR_button;
    QTextEdit *resultado1;
    QTextEdit *resultado2;
    QTextEdit *resultado3;
    QPushButton *GLOBAL_button;
    QFrame *viewer_counter1_umbral;
    QFrame *viewer_counter2_umbral;
    QFrame *viewer_counter3_umbral;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;

    void setupUi(QWidget *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->setEnabled(true);
        MainWindow->resize(1431, 836);
        MainWindow->setMouseTracking(true);
        MainWindow->setAcceptDrops(true);
        QIcon icon;
        icon.addFile(QString::fromUtf8("../../../Digital_books_and_other_things/TESIS_PACO/goeiti.jpg"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        MainWindow->setWindowOpacity(1);
        viewer_original = new QFrame(MainWindow);
        viewer_original->setObjectName(QString::fromUtf8("viewer_original"));
        viewer_original->setEnabled(true);
        viewer_original->setGeometry(QRect(10, 20, 720, 540));
        QPalette palette;
        QBrush brush(QColor(240, 29, 14, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Highlight, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Highlight, brush);
        QBrush brush1(QColor(240, 240, 240, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::Highlight, brush1);
        viewer_original->setPalette(palette);
        viewer_original->setFrameShape(QFrame::StyledPanel);
        viewer_original->setFrameShadow(QFrame::Raised);
        viewer_original->setLineWidth(2);
        Tittle = new QLabel(MainWindow);
        Tittle->setObjectName(QString::fromUtf8("Tittle"));
        Tittle->setEnabled(true);
        Tittle->setGeometry(QRect(310, 0, 131, 20));
        QPalette palette1;
        QBrush brush2(QColor(0, 0, 255, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush2);
        QBrush brush3(QColor(45, 242, 35, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Button, brush3);
        QBrush brush4(QColor(255, 0, 0, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Text, brush4);
        palette1.setBrush(QPalette::Active, QPalette::ButtonText, brush2);
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush2);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush3);
        palette1.setBrush(QPalette::Inactive, QPalette::Text, brush4);
        QBrush brush5(QColor(76, 76, 76, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Inactive, QPalette::ButtonText, brush5);
        QBrush brush6(QColor(159, 158, 158, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush6);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush3);
        palette1.setBrush(QPalette::Disabled, QPalette::Text, brush6);
        palette1.setBrush(QPalette::Disabled, QPalette::ButtonText, brush6);
        Tittle->setPalette(palette1);
        QFont font;
        font.setItalic(true);
        Tittle->setFont(font);
        Tittle->setCursor(QCursor(Qt::PointingHandCursor));
        Load_button = new QPushButton(MainWindow);
        Load_button->setObjectName(QString::fromUtf8("Load_button"));
        Load_button->setGeometry(QRect(70, 580, 121, 27));
        viewer_counter1 = new QFrame(MainWindow);
        viewer_counter1->setObjectName(QString::fromUtf8("viewer_counter1"));
        viewer_counter1->setEnabled(true);
        viewer_counter1->setGeometry(QRect(750, 20, 304, 130));
        QPalette palette2;
        palette2.setBrush(QPalette::Active, QPalette::Highlight, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::Highlight, brush);
        palette2.setBrush(QPalette::Disabled, QPalette::Highlight, brush1);
        viewer_counter1->setPalette(palette2);
        viewer_counter1->setFrameShape(QFrame::StyledPanel);
        viewer_counter1->setFrameShadow(QFrame::Raised);
        viewer_counter1->setLineWidth(2);
        label_2 = new QLabel(MainWindow);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(750, 0, 191, 17));
        Clip_button = new QPushButton(MainWindow);
        Clip_button->setObjectName(QString::fromUtf8("Clip_button"));
        Clip_button->setGeometry(QRect(240, 580, 141, 27));
        viewer_counter2 = new QFrame(MainWindow);
        viewer_counter2->setObjectName(QString::fromUtf8("viewer_counter2"));
        viewer_counter2->setEnabled(true);
        viewer_counter2->setGeometry(QRect(750, 260, 304, 130));
        QPalette palette3;
        palette3.setBrush(QPalette::Active, QPalette::Highlight, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::Highlight, brush);
        palette3.setBrush(QPalette::Disabled, QPalette::Highlight, brush1);
        viewer_counter2->setPalette(palette3);
        viewer_counter2->setFrameShape(QFrame::StyledPanel);
        viewer_counter2->setFrameShadow(QFrame::Raised);
        viewer_counter2->setLineWidth(2);
        label_3 = new QLabel(MainWindow);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(750, 240, 191, 17));
        viewer_counter3 = new QFrame(MainWindow);
        viewer_counter3->setObjectName(QString::fromUtf8("viewer_counter3"));
        viewer_counter3->setEnabled(true);
        viewer_counter3->setGeometry(QRect(750, 500, 304, 130));
        QPalette palette4;
        palette4.setBrush(QPalette::Active, QPalette::Highlight, brush);
        palette4.setBrush(QPalette::Inactive, QPalette::Highlight, brush);
        palette4.setBrush(QPalette::Disabled, QPalette::Highlight, brush1);
        viewer_counter3->setPalette(palette4);
        viewer_counter3->setFrameShape(QFrame::StyledPanel);
        viewer_counter3->setFrameShadow(QFrame::Raised);
        viewer_counter3->setLineWidth(2);
        label_4 = new QLabel(MainWindow);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(750, 480, 191, 17));
        OCR_button = new QPushButton(MainWindow);
        OCR_button->setObjectName(QString::fromUtf8("OCR_button"));
        OCR_button->setGeometry(QRect(440, 580, 151, 27));
        resultado1 = new QTextEdit(MainWindow);
        resultado1->setObjectName(QString::fromUtf8("resultado1"));
        resultado1->setGeometry(QRect(1070, 70, 151, 31));
        resultado2 = new QTextEdit(MainWindow);
        resultado2->setObjectName(QString::fromUtf8("resultado2"));
        resultado2->setGeometry(QRect(1070, 310, 141, 31));
        resultado3 = new QTextEdit(MainWindow);
        resultado3->setObjectName(QString::fromUtf8("resultado3"));
        resultado3->setGeometry(QRect(1070, 550, 141, 31));
        GLOBAL_button = new QPushButton(MainWindow);
        GLOBAL_button->setObjectName(QString::fromUtf8("GLOBAL_button"));
        GLOBAL_button->setGeometry(QRect(250, 620, 121, 27));
        viewer_counter1_umbral = new QFrame(MainWindow);
        viewer_counter1_umbral->setObjectName(QString::fromUtf8("viewer_counter1_umbral"));
        viewer_counter1_umbral->setEnabled(true);
        viewer_counter1_umbral->setGeometry(QRect(30, 690, 304, 130));
        QPalette palette5;
        palette5.setBrush(QPalette::Active, QPalette::Highlight, brush);
        palette5.setBrush(QPalette::Inactive, QPalette::Highlight, brush);
        palette5.setBrush(QPalette::Disabled, QPalette::Highlight, brush1);
        viewer_counter1_umbral->setPalette(palette5);
        viewer_counter1_umbral->setFrameShape(QFrame::StyledPanel);
        viewer_counter1_umbral->setFrameShadow(QFrame::Raised);
        viewer_counter1_umbral->setLineWidth(2);
        viewer_counter2_umbral = new QFrame(MainWindow);
        viewer_counter2_umbral->setObjectName(QString::fromUtf8("viewer_counter2_umbral"));
        viewer_counter2_umbral->setEnabled(true);
        viewer_counter2_umbral->setGeometry(QRect(390, 690, 304, 130));
        QPalette palette6;
        palette6.setBrush(QPalette::Active, QPalette::Highlight, brush);
        palette6.setBrush(QPalette::Inactive, QPalette::Highlight, brush);
        palette6.setBrush(QPalette::Disabled, QPalette::Highlight, brush1);
        viewer_counter2_umbral->setPalette(palette6);
        viewer_counter2_umbral->setFrameShape(QFrame::StyledPanel);
        viewer_counter2_umbral->setFrameShadow(QFrame::Raised);
        viewer_counter2_umbral->setLineWidth(2);
        viewer_counter3_umbral = new QFrame(MainWindow);
        viewer_counter3_umbral->setObjectName(QString::fromUtf8("viewer_counter3_umbral"));
        viewer_counter3_umbral->setEnabled(true);
        viewer_counter3_umbral->setGeometry(QRect(750, 690, 304, 130));
        QPalette palette7;
        palette7.setBrush(QPalette::Active, QPalette::Highlight, brush);
        palette7.setBrush(QPalette::Inactive, QPalette::Highlight, brush);
        palette7.setBrush(QPalette::Disabled, QPalette::Highlight, brush1);
        viewer_counter3_umbral->setPalette(palette7);
        viewer_counter3_umbral->setFrameShape(QFrame::StyledPanel);
        viewer_counter3_umbral->setFrameShadow(QFrame::Raised);
        viewer_counter3_umbral->setLineWidth(2);
        label_5 = new QLabel(MainWindow);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(80, 670, 191, 17));
        label_6 = new QLabel(MainWindow);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(460, 670, 191, 17));
        label_7 = new QLabel(MainWindow);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(820, 670, 191, 17));

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QWidget *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "COUNTERS OCR ", 0, QApplication::UnicodeUTF8));
        Tittle->setText(QApplication::translate("MainWindow", "Source Frame", 0, QApplication::UnicodeUTF8));
        Load_button->setText(QApplication::translate("MainWindow", "Load  Image", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWindow", "Clipping 1", 0, QApplication::UnicodeUTF8));
        Clip_button->setText(QApplication::translate("MainWindow", "Clipping", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("MainWindow", "Clipping 2", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("MainWindow", "Clipping 3", 0, QApplication::UnicodeUTF8));
        OCR_button->setText(QApplication::translate("MainWindow", "Extract Numbers", 0, QApplication::UnicodeUTF8));
        GLOBAL_button->setText(QApplication::translate("MainWindow", "Global Process", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("MainWindow", "Threshold Counter 1", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("MainWindow", "Threshold Counter 2", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("MainWindow", "Threshold Counter 3", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
